#!/usr/bin/env python3

import math
from docopt import docopt
import coloredlogs
import logging
import json
import requests
import itertools

args = docopt("""Usage: ./gitlab-mr-tables.py <id> [-t TOKEN] [-u URL] [--debug]

-h --help           show this
-t --token=<TOKEN>  Gitlab personal access token
-u --url=<URL>      URL of the Gitlab instance [default: https://gitlab.com]
-d --debug          Whether to enable debugging
""")

#
# UTILITY FUNCTIONS
#
class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__
    def __init__(self, dct):
        for key, value in dct.items():
            if hasattr(value, 'keys'):
                value = dotdict(value)
            self[key] = value

# Create a logger object.
logger = logging.getLogger(__name__)

coloredlogs.install(level=('DEBUG' if args['--debug'] else 'INFO'))

logger.info("Loaded table generator with " + json.dumps(args))

id = args['<id>']
gitlabUrl = args['--url']

resp = requests.get("%s/api/v4/projects/%s/merge_requests?state=opened&private_token=%s" % (gitlabUrl, id, args['--token']))
mergeRequests = resp.json()
#logger.debug(json.dumps(mergeRequests, indent = 2))

#TODO: Use pytablewriter
print("| Coder | Merge Request | Status | Reviewer #1 | Reviewer #2|")
print("|---|---|---|---|---|")

for mr in mergeRequests:
    mr = dotdict(mr)
    logger.debug("Found merge request %i (%s)" % (mr.iid, mr.title))

    # Query information about this MR
    resp = requests.get(
        "%s/api/v4/projects/%s/merge_requests/%i/notes?private_token=%s" % (
        gitlabUrl, id, mr.iid, args['--token']))
    notes = list(map(lambda x: dotdict(x), resp.json()))
    resp = requests.get(
        "%s/api/v4/projects/%s/merge_requests/%i/discussions?private_token=%s" % (gitlabUrl, id, mr.iid, args['--token']))
    discussions = list(map(lambda x: dotdict(x), resp.json()))
    resp = requests.get(
        "%s/api/v4/projects/%s/merge_requests/%i/approvals?private_token=%s" % (
        gitlabUrl, id, mr.iid, args['--token']))
    approvals = list(map(lambda x: dotdict(x), resp.json()["approved_by"]))
    #logger.debug(json.dumps(notes, indent=2))

    # Count notes by author
    authors = {}
    discussions.append(dotdict({"notes": notes}))
    for discussion in discussions:
        for note in discussion.notes:
            note = dotdict(note)
            if note.author.id not in authors:
                authors[note.author.id] = note.author
                authors[note.author.id].occ = 0
                authors[note.author.id].approved = False
            authors[note.author.id].occ += 1
        sortedAuthors = filter(lambda a: a != mr.author.id, authors)
        sortedAuthors = sorted(sortedAuthors, key=lambda a: -authors[a].occ)
    for approval in approvals:
        approval = dotdict(approval)
        if approval.user.id not in authors:
            authors[approval.user.id] = approval.user
        authors[approval.user.id].occ = math.inf # Infinity for approvers, always show them
        authors[approval.user.id].approved = True

    # Formatting for labels
    labels = list(map(lambda l: l.lower(), mr.labels))
    labelString = ""
    if "pending review" in labels:
        labelString += ":large_blue_circle: "
    if "pending approval" in labels:
        labelString += ":large_blue_diamond: "
    if "waiting for code" in labels:
        labelString += ":construction: "
    if "blocked by another mr" in labels:
        labelString += ":stop_sign: "
    if "good to merge!" in labels:
        labelString += ":heavy_check_mark: "
    if "urgent" in labels:
        labelString += ":warning: "
    labelString += ', '.join(mr.labels)

    # Now we can print
    print("| ![%s](%s =32px \"%s\") | [!%i %s](%s) | %s " %
          (
              mr.author.name,
              mr.author.avatar_url,
              mr.author.name,
              mr.iid,
              mr.title.replace("|","\\|"),
              mr.web_url,
              labelString
          ), end=''
          )

    i = 0
    for a in sortedAuthors:
        if i >= 2: break
        author = authors[a]

        logger.debug("Adding %s with %f notes" % (author.name, author.occ ))
        print("| %s![%s](%s =32px \"%s\") " % (
              ":red_circle: " if not author.approved and "work in progress" not in labels else "",
              author.name, author.avatar_url, author.name), end=''
              )

        i += 1
    while i < 2:
        print("| *you can too!* ", end='')
        i += 1
    print("|")





# gitlab-mr-tables

Generates **Markdown tables** with information for open **GitLab merge requests**.

## Installation instructions
Make sure you have **Python 3** installed in your system.
```bash
pip3 install -r requirements.txt
python3 gitlab-mr-tables.py
```

